$(document).ready(function(){
	headerBackground($(window).scrollTop())
	onBodyLoad()
	
	let coverTextTop = parseInt($("#cover-text").css("top") , 10)	
	$(window).scroll(function(){
	let scroll = $(window).scrollTop()

	coverEffect(coverTextTop, scroll)
	headerBackground(scroll)
	})


	//mobile menu icon functionality
	$("#mobile-menu-icon").click(function(){
		if( $("header nav").css("display") == "none" ){
			$("header nav").slideToggle(500)
			headerBackground(0)
			$('#mobile-menu-icon span:nth-child(1)').addClass("first-span")
			$('#mobile-menu-icon span:nth-child(2)').addClass("second-span")
			$('#mobile-menu-icon span:nth-child(3)').addClass("third-span")
			$('#mobile-menu-icon').addClass("mobile-menu-icon-active")
		}
		else{
			closeMobileMenu()
		}
	})


//make links with script
	// $("a[href^='#']").click(function(e) {
	// e.preventDefault();
	
	// var position = $($(this).attr("href")).offset().top;

	// $("body, html").animate({
	// 	scrollTop: position
	// } /* speed */ );
	// })

	//in, because we need to set coverTextTop variable every time on resize
	// $(window).resize(function(){
	// 	onBodyLoad()
		
	// 	coverTextTop = parseInt($("#cover-text").css("top") , 10)	
	// 	let scroll = $(window).scrollTop()
	// 	coverEffect(coverTextTop, scroll)
	// })
})


// this defines the effect that makes cover moves up and text down, like they are on different layers
function coverEffect(coverTextHeight, scroll){
	$("#cover > img").css("top", - scroll/3);
	$("#cover-text").css("top", coverTextHeight + scroll/2 )
}

//this function gives the background of the header
function headerBackground(scroll){
	if(scroll > $("#cover").height()/2 || ($("#mobile-menu-icon").css("display") != "none" && $("header nav").css("display") != "none") ){
		$("header").addClass("header-shadow")
		$("header").removeClass("transparent")
		$("#logo > a >img").attr("src", "images/logo-black.png")
		$("#logo > a >img").css("width", "80%")
		}
	else{
		$("header").removeClass("header-shadow")
		$("header").addClass("transparent")
		$("#logo > a > img").attr("src", "images/logo-white.png")	
		$("#logo > a >img").css("width", "100%")
	}
}


function setArticlesMinHeight(){

	if($(window).width()>768){
		// header-heights
		let headerHeights = []
		$(".article-header").each(function(index, header){
			headerHeights[index] = header.clientHeight
		})
		$(".article-header").each(function(index, header){
			header.style.minHeight = Math.max(...headerHeights) + "px"
		})
		
		// content-heights
		let contentHeights = []
		$(".article-content-wrapper").each(function(index, header){
			headerHeights[index] = header.clientHeight
		})
		$(".article-content-wrapper").each(function(index, header){
			header.style.minHeight = Math.max(...headerHeights) + "px"
		})
	}
	else{
		$(".article-header").each(function(index, header){
			header.style.minHeight = 0 + "px"
		})
		$(".article-content-wrapper").each(function(index, header){
			header.style.minHeight = 0 + "px"
		})
	}
}

function fixMobileMenuButtonPosition(headerHeight){
	let btn = $("#mobile-menu-icon")
	let moveDown = (headerHeight - btn.height())/2
	btn.css("top", moveDown)
		.css("right", moveDown)
}

function closeMobileMenu(){
	$('#mobile-menu-icon span:nth-child(1)').removeClass("first-span")
	$('#mobile-menu-icon span:nth-child(2)').removeClass("second-span")
	$('#mobile-menu-icon span:nth-child(3)').removeClass("third-span")
	$('#mobile-menu-icon').removeClass("mobile-menu-icon-active")
	$.when($("header nav").slideUp(500)).done(function(){
		headerBackground($(window).scrollTop())
	})
}


function onBodyLoad(){
	
	setArticlesMinHeight()

		// fix cover size
	if($("#cover").height() >= $("#cover img").height()){
		$("#cover").height($("#cover img").height())
	}
	else{
		$("#cover").height($(window).height())	
	}

	// cover text position
	$("#cover-text").css("top", ($("#cover").height() - $("#cover-text").height() )/2)

	// make clicking li in menu closing the menu
	if($("#mobile-menu-icon").css("display") != "none"){
		$("nav ul li :nth-child(n)").click(function(){
			closeMobileMenu();
		})
	}

	// mobile menu fix
	fixMobileMenuButtonPosition($("header").height())

}